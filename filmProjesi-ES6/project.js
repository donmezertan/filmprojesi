const form = document.getElementById("film-form");
const titleElement = document.querySelector("#title");
const directorElement = document.querySelector("#director");
const urlElement = document.querySelector("#url");
const cardBody = document.querySelectorAll(".card-body")[1];
const clearButton = document.getElementById("clear-films");
eventListeners();

function eventListeners() {
    form.addEventListener("submit", addFilm);
    document.addEventListener("DOMContentLoaded", function () {
        let films = Storage.getFilmsFromStorage();
        UI.loadAllFilms(films);
    });
    cardBody.addEventListener("click", deleteFilm);
    clearButton.addEventListener("click", clearAllFilms)
};

function addFilm(e) {
    const title = titleElement.value;
    const director = directorElement.value;
    const url = urlElement.value;
    if (title === "" || director === "" || url === "") {
        //Hata Mesajları
        UI.displayMessage("Tüm Alanları doldurun", "danger");

    } else {
        //Yeni Film
        const newFilm = new Film(title, director, url);
        UI.addFilmToUI(newFilm); //Arayüze Ekleme
        Storage.addFilmToStorage(newFilm); //Storage Film Ekleme

        UI.displayMessage("Filminiz Başarıyla Eklendi", "success");
    }
    UI.clearInputs(titleElement, directorElement, urlElement);
    e.preventDefault();
};

function deleteFilm(e) {
    if (e.target.id === "delete-film") {
        UI.deleteFilmFromUI(e.target);
        Storage.deteleFilmFromStorage(e.target.parentElement.previousElementSibling.previousElementSibling.textContent);
        UI.displayMessage("Silme İşlemi başarılı", "succes");
    }

};

function clearAllFilms() {
    if (confirm("Emin misiniz ?")) {
        UI.clearAllFilmsFromUI();
       Storage.clearAllFilmsFromStorage();
    }
};

